import React, { useState } from "react";
import axios from "axios";

const ImageUpload = () => {
  const [image, setImage] = useState(null);
  const [imageURL, setImageURL] = useState(null);
  const [ingredients, setIngredients] = useState([]);
  const PAT = "c790c782c7504c4091d1982e45f1ce31";
  const USER_ID = "clarifai";
  const APP_ID = "main";
  const MODEL_ID = "food-item-recognition";
  const MODEL_VERSION_ID = "1d5fd481e0cf4826aa72ec3ff049e044";

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setImage(file);

    // Générer une URL pour afficher l'image sélectionnée
    const reader = new FileReader();
    reader.onloadend = () => {
      setImageURL(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const handleUpload = async () => {
    const reader = new FileReader();
    reader.onloadend = async () => {
      const base64Image = reader.result.split(",")[1];

      const data = {
        user_app_id: {
          user_id: USER_ID,
          app_id: APP_ID,
        },
        inputs: [
          {
            data: {
              image: {
                base64: base64Image,
              },
            },
          },
        ],
      };

      try {
        const response = await axios.post(
          `https://api.clarifai.com/v2/models/${MODEL_ID}/versions/${MODEL_VERSION_ID}/outputs`,
          data,
          {
            headers: {
              Accept: "application/json",
              Authorization: `Key ${PAT}`,
              "Content-Type": "application/json",
            },
          }
        );
        const detectedIngredients = response.data.outputs[0].data.concepts;
        setIngredients(detectedIngredients.map((concept) => concept.name));
      } catch (error) {
        console.error("Error detecting ingredients:", error);
      }
    };
    reader.readAsDataURL(image);
  };

  return (
    <div>
      <input type="file" accept="image/*" onChange={handleImageChange} />
      {imageURL && (
        <img
          src={imageURL}
          alt="Selected"
          style={{ width: "300px", marginTop: "20px" }}
        />
      )}
      <button onClick={handleUpload}>Analiser l'image</button>
      <div>
        {ingredients.length > 0 && (
          <ul>
            {ingredients.map((ingredient, index) => (
              <li key={index} style={{ listStyle: "none" }}>
                {ingredient}
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
};

export default ImageUpload;
