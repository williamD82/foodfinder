import React, { useState } from "react";
import axios from "axios";

const VideoUpload = () => {
  const [video, setVideo] = useState(null);
  const [videoURL, setVideoURL] = useState(null);
  const [ingredients, setIngredients] = useState([]);
  const PAT = "c790c782c7504c4091d1982e45f1ce31";
  const USER_ID = "clarifai";
  const APP_ID = "main";
  const MODEL_ID = "food-item-recognition";
  const MODEL_VERSION_ID = "1d5fd481e0cf4826aa72ec3ff049e044";

  const handleVideoChange = (e) => {
    const file = e.target.files[0];
    setVideo(file);
    setVideoURL(URL.createObjectURL(file));
  };

  const extractFramesAndDetectIngredients = async (videoFile) => {
    const videoElement = document.createElement("video");
    videoElement.src = URL.createObjectURL(videoFile);

    const frameRate = 1; // Analyzing one frame per second
    const ingredientsSet = new Set();

    videoElement.addEventListener("loadeddata", async () => {
      const canvas = document.createElement("canvas");
      const context = canvas.getContext("2d");

      videoElement.currentTime = 0;

      while (videoElement.currentTime < videoElement.duration) {
        context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
        const imageUrl = canvas.toDataURL("image/jpeg");

        const data = {
          user_app_id: {
            user_id: USER_ID,
            app_id: APP_ID,
          },
          inputs: [
            {
              data: {
                image: {
                  base64: imageUrl.split(",")[1], // Send only the base64 part of the Data URL
                },
              },
            },
          ],
        };

        try {
          const response = await axios.post(
            `https://api.clarifai.com/v2/models/${MODEL_ID}/versions/${MODEL_VERSION_ID}/outputs`,
            data,
            {
              headers: {
                Accept: "application/json",
                Authorization: `Key ${PAT}`,
                "Content-Type": "application/json",
              },
            }
          );
          const result = response.data;
          const detectedIngredients = result.outputs[0].data.concepts;
          detectedIngredients.forEach((concept) =>
            ingredientsSet.add(concept.name)
          );
        } catch (error) {
          console.error("Error detecting ingredients:", error);
        }

        videoElement.currentTime += frameRate;
      }

      setIngredients(Array.from(ingredientsSet));
    });

    videoElement.load();
  };

  const handleUpload = () => {
    if (video) {
      extractFramesAndDetectIngredients(video);
    }
  };

  return (
    <div>
      <input type="file" accept="video/*" onChange={handleVideoChange} />
      {videoURL && (
        <video
          src={videoURL}
          controls
          style={{ width: "300px", marginTop: "20px" }}
        />
      )}
      <button onClick={handleUpload}>Upload Video</button>
      <div>
        {ingredients.length > 0 && (
          <ul>
            {ingredients.map((ingredient, index) => (
              <li key={index} style={{ listStyle: "none" }}>
                {ingredient}
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
};

export default VideoUpload;
