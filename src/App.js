import "./App.css";
import { APP_NAME } from "./commons/constants";
import ImageUpload from "./components/ImageUpload";
import VideoUpload from "./components/VideoUpload";

function App() {
  return (
    <div className="App">
      <h1>{APP_NAME}</h1>
      <h3>Analyseur d'ingrédients de la recette</h3>
      <div>
        <p>Tester une Vidéo</p>
        <VideoUpload />
      </div>
      <div style={{ margin: "90px" }}></div>
      <div>
        <p>Tester une Image</p>
        <ImageUpload />
      </div>
    </div>
  );
}

export default App;
